import os
import csv
import models
import json

from flask import Flask, request, make_response, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump
from dotenv import load_dotenv
from typing import Dict, List, Set
from db_setup import drop_db, create_db
from models import Cities, Colleges, Companies


def read_csv(relative_path: str) -> List[Dict[str, str]]:
    """helper method which returns a list of dicts of the CSV's contents,
    note that one dict is used per row"""
    dirname = os.path.dirname(__file__)
    csv_file_path = os.path.join(dirname, relative_path)

    ret = None
    with open(csv_file_path) as file:
        reader = csv.DictReader(file) # iterate over all entries in the CSV file
        ret = list(reader)

    return ret


def make_cities_set(relative_path: str, key: str):
    """relative_path is the path to the CSV and the key is the column header of the city col"""
    cities = set()
    parsed_csv = read_csv(relative_path)

    for model_dict in parsed_csv:
        cities.add(model_dict[key]) # fill the set with all the city names
    return cities


def populate_city(
    db,
    city_dict: Dict[str, str],
    visited: set,
    business_cities: set,
    college_cities: set,
):
    """method to populate a city model instance using information from the parsed CSV"""

    entry = {} # # dictionary form of the entry used to organize all the city data 

    check_key = lambda dict, key: dict[key] if dict[key] != "No data" else None

    try:
        entry["id"] = city_dict["geoname_id"]

        # Filtering out data.

        if city_dict["name"] in visited:
            return
        if city_dict["full_name"].split(",")[2].strip() != "United States":
            return
        if city_dict["web_image"] == "No data":
            return
        if city_dict["name"] not in college_cities:
            return

        # Searchables.
        entry["name"] = city_dict["name"]
        visited.add(entry["name"])
        entry["state"] = city_dict["full_name"].split(",")[1].strip()
        entry["full_name"] = city_dict["full_name"]

        entry["population"] = city_dict["population"]
        entry["timezone"] = None
        entry["latitude"] = city_dict["latitude"]
        entry["longitude"] = city_dict["longitude"]

        # Indices.
        entry["housing"] = check_key(city_dict, "Housing")
        entry["cost_of_living"] = check_key(city_dict, "Cost of Living")
        entry["startups"] = check_key(city_dict, "Startups")
        entry["commute"] = check_key(city_dict, "Commute")
        entry["business_freedom"] = check_key(city_dict, "Business Freedom")
        entry["safety"] = check_key(city_dict, "Safety")
        entry["healthcare"] = check_key(city_dict, "Healthcare")
        entry["education"] = check_key(city_dict, "Education")
        entry["environmental_quality"] = check_key(city_dict, "Environmental Quality")
        entry["economy"] = check_key(city_dict, "Economy")
        entry["taxation"] = check_key(city_dict, "Taxation")
        entry["internet_access"] = check_key(city_dict, "Internet Access")
        entry["leisure_culture"] = check_key(city_dict, "Leisure & Culture")
        entry["tolerance"] = check_key(city_dict, "Tolerance")
        entry["outdoors"] = check_key(city_dict, "Outdoors")
        entry["travel_connectivity"] = check_key(city_dict, "Travel Connectivity")

        entry["web_image"] = check_key(city_dict, "web_image")

        entry["mobile_image"] = check_key(city_dict, "mobile_image")

        city_instance = Cities(**entry) # construct the Cities instance, where Cities is a SQL Alchemy model

        db.session.add(city_instance)

    except Exception as e:
        print(e)


def populate_cities(
    db, parsed_csv: List[Dict[str, str]], business_cities: set, college_cities: set
) -> Set[str]:
    """method to populate all city model instances that will be used for this project"""

    if parsed_csv is None:
        print("Missing data.")
        return

    visited = set()
    for city_dict in parsed_csv:
        populate_city(db, city_dict, visited, business_cities, college_cities)
    db.session.commit()
    return visited


def populate_college(db, college_dict: Dict[str, str], visited: set):
    """method to populate a college model instance using information from the parsed CSV"""
    entry = {} # dictionary form of the entry used to organize all the college data

    check_key = lambda dict, key: None if dict[key] == "" else dict[key]

    try:

        # Searchables
        entry["name"] = college_dict["school.name"]
        if entry["name"] in visited:
            return
        visited.add(entry["name"])
        entry["student_size"] = check_key(college_dict, "student.size")
        entry["school_school_url"] = check_key(college_dict, "school.school_url")
        entry["school_city"] = check_key(college_dict, "school.city")
        entry["school_zip"] = check_key(college_dict, "school.zip")
        entry["location_lat"] = college_dict["location.lat"]
        entry["location_lon"] = college_dict["location.lon"]

        # Indices.
        entry["student_demographics_women"] = check_key(
            college_dict, "student.demographics.women"
        )
        entry["student_demographics_men"] = check_key(
            college_dict, "student.demographics.men"
        )
        entry["admissions_admission_rate_overall"] = check_key(
            college_dict, "admissions.admission_rate.overall"
        )
        entry["admissions_sat_scores_average_overall"] = check_key(
            college_dict, "admissions.sat_scores.average.overall"
        )
        entry["admissions_act_scores_midpoint_cumulative"] = check_key(
            college_dict, "admissions.act_scores.midpoint.cumulative"
        )
        entry["student_demographics_race_ethnicity_white"] = check_key(
            college_dict, "student.demographics.race_ethnicity.white"
        )
        entry["student_demographics_race_ethnicity_black"] = check_key(
            college_dict, "student.demographics.race_ethnicity.black"
        )
        entry["student_demographics_race_ethnicity_hispanic"] = check_key(
            college_dict, "student.demographics.race_ethnicity.hispanic"
        )
        entry["student_demographics_race_ethnicity_asian"] = check_key(
            college_dict, "student.demographics.race_ethnicity.asian"
        )
        entry["student_demographics_race_ethnicity_aian"] = check_key(
            college_dict, "student.demographics.race_ethnicity.aian"
        )
        entry["student_demographics_race_ethnicity_nhpi"] = check_key(
            college_dict, "student.demographics.race_ethnicity.nhpi"
        )
        entry["student_demographics_race_ethnicity_unknown"] = check_key(
            college_dict, "student.demographics.race_ethnicity.unknown"
        )
        entry["student_demographics_race_ethnicity_two_or_more"] = check_key(
            college_dict, "student.demographics.race_ethnicity.two_or_more"
        )
        entry["student_share_firstgeneration"] = check_key(
            college_dict, "student.share_firstgeneration"
        )
        entry["school_carnegie_undergrad"] = check_key(
            college_dict, "school.carnegie_undergrad"
        )
        entry["earnings_6_yrs_after_entry_median"] = check_key(
            college_dict, "earnings.6_yrs_after_entry.median"
        )
        entry["student_demographics_avg_family_income"] = check_key(
            college_dict, "student.demographics.avg_family_income"
        )
        entry["student_retention_rate_four_year_full_time_pooled"] = check_key(
            college_dict, "student.retention_rate.four_year.full_time_pooled"
        )
        entry["cost_tuition_in_state"] = check_key(
            college_dict, "cost.tuition.in_state"
        )
        entry["cost_tuition_out_of_state"] = check_key(
            college_dict, "cost.tuition.out_of_state"
        )
        entry["image"] = check_key(college_dict, "image")

        college_instance = Colleges(**entry)
        db.session.add(college_instance)

    except Exception as e:
        print(e)


def populate_colleges(db, parsed_csv: List[Dict[str, str]], visited_cities: Set[str]):
    """method to populate all college model instances that will be used for this project"""
    if parsed_csv is None:
        print("Missing data.")
        return Set() # get a set of all of the colleges

    visited = set()
    for college_dict in parsed_csv:
        if college_dict["school.city"] not in visited_cities:
            continue  # skip over colleges that don't have a known associated city with them
        populate_college(db, college_dict, visited)
    db.session.commit()


def populate_company(db, company_dict: Dict[str, str], visited: set, another_cities):
    """method to populate a company model instance using information from the parsed CSV"""

    entry = {} # used to organize company information.

    check_key = lambda dict, key: dict[key] if dict[key] != "" else None

    try:
        entry["company_id"] = company_dict["id"]

        # Filtering out data.
        if (
            company_dict["name"] in visited
            or company_dict["location.country"] != "United States"
        ):
            return
        if (
            company_dict["location.latitude"] == ""
            or company_dict["location.longitude"] == ""
            or company_dict["name"] == ""
        ):
            return
        another_cities.add(company_dict["location.city"])

        # Searchables
        entry["name"] = company_dict["name"]
        visited.add(entry["name"])
        entry["founded_year"] = check_key(company_dict, "founded_year")
        entry["location_street_name"] = check_key(company_dict, "location.street_name")
        entry["location_street_number"] = check_key(
            company_dict, "location.street_number"
        )
        entry["location_sub_premise"] = check_key(company_dict, "location.sub_premise")
        entry["location_city"] = company_dict["location.city"]
        entry["location_postal_code"] = check_key(company_dict, "location.postal_code")
        entry["locaton_state"] = company_dict["location.state"]
        entry["location_state_code"] = company_dict["location.state_code"]
        entry["location_country"] = company_dict["location.country"]
        entry["location_country_code"] = company_dict["location.country_code"]
        entry["location_latitude"] = company_dict["location.latitude"]
        entry["location_longitude"] = company_dict["location.longitude"]
        entry["logo"] = check_key(company_dict, "image")
        entry["phone_number"] = check_key(company_dict, "phone_number")
        entry["email"] = check_key(company_dict, "email_address")
        entry["twitter_handle"] = check_key(company_dict, "twitter_handle")
        entry["alexa_rank"] = check_key(company_dict, "alexa_us_rank")

        # Filterable
        entry["category_tags"] = company_dict["category_tags"]
        entry["industry"] = company_dict["industry"]
        entry["employees"] = check_key(company_dict, "employees")
        entry["estimated_annual_revenue"] = check_key(
            company_dict, "estimated_annual_revenue"
        )
        entry["twitter_id"] = check_key(company_dict, "twitter_id")
        entry["linkedin_handle"] = check_key(company_dict, "linkedin_handle")
        entry["description"] = check_key(company_dict, "description")
        category_list = check_key(
            company_dict, "category_tags"
        )  # this is a list to deal with from csv

        entry["category_tags"] = (
            None if category_list is None else json.loads(category_list)
        )
        company_instance = Companies(**entry)
        db.session.add(company_instance)
        db.session.commit()

    except Exception as e:
        print(e)


def populate_companies(db, parsed_csv: List[Dict[str, str]], visited_cities: Set[str]):
    """method to populate all company model instances that will be used for this project"""
    if parsed_csv is None:
        print("Missing data")
        return Set()

    visited = set()
    another = set()
    for company_dict in parsed_csv:
        if company_dict["location.city"] not in visited_cities:
            continue
        populate_company(db, company_dict, visited, another)

    print(visited_cities - another)
    print(len(visited_cities - another))

    # db.session.commit()


def populate_cities_model_connections(db):
    """method to populate all city model instances that will be used for this project"""

    cities = db.session.query(Cities).all()
    colleges = db.session.query(Colleges).all()

    cache = (
        {}
    )  # city id: dict of college ids ex: 5107129: {'1': '166018', '2': '164465', '3': '190725', '4': '166629'}

    for college in colleges:
        college_city = college.city_id
        if college_city not in cache:
            cache[college_city] = {}
        the_key = str(
            len(cache[college_city]) + 1
        )  # i.e. 1, 2, 3, ... will be assigned
        cache[college_city][the_key] = college.id

    for city in cities:
        city.colleges_ids = json.dumps(cache[city.id])

    cache = {}  # city id: dict of company ids
    companies = db.session.query(Companies).all()

    for company in companies:
        company_city = company.city_id
        if company_city not in cache:
            cache[company_city] = {}
        the_key = str(
            len(cache[company_city]) + 1
        )  # i.e. 1, 2, 3, ... will be assigned
        cache[company_city][the_key] = company.company_id

    for city in cities:
        city.companies_ids = json.dumps(cache[city.id])

    db.session.commit()
    return


def populate_colleges_companies_model_connections(
    db, threshold=300.0
):  # Note: put default threshold here for easy use.
    """method to populate all college <--> company connections
    Haversine part, credit to: Nathan A. Rooy, Haversine Formula, June, 2016 (we had to change it a bit though)
    link: https://nathanrooy.github.io/posts/2016-09-07/haversine-with-python/"""
    import math

    def compute_haversine(lon1, lat1, lon2, lat2):
        """computes the haversine distance between two locations (1 and 2) and returns
        their distances in miles"""
        R = 6371000  # radius of Earth in meters
        phi_1 = math.radians(lat1)
        phi_2 = math.radians(lat2)

        delta_phi = math.radians(lat2 - lat1)
        delta_lambda = math.radians(lon2 - lon1)

        a = (
            math.sin(delta_phi / 2.0) ** 2
            + math.cos(phi_1) * math.cos(phi_2) * math.sin(delta_lambda / 2.0) ** 2
        )
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

        meters = R * c  # output distance in meters
        km = meters / 1000.0  # output distance in kilometers
        miles = meters * 0.000621371  # output distance in miles
        feet = miles * 5280  # output distance in feet
        return miles

    companies = db.session.query(Companies).all()
    colleges = db.session.query(Colleges).all()

    companies_cache = {}  # company id: {'1': <first college id>, ...}
    colleges_cache = {}  # college id: {'1': <first company id>, ...}

    for company in companies:
        for college in colleges:
            company_lon, company_lat = (
                company.location_longitude,
                company.location_latitude,
            )
            college_lon, college_lat = college.location_lon, college.location_lat
            dist = abs(
                compute_haversine(company_lon, company_lat, college_lon, college_lat)
            )
            if (
                company.company_id not in companies_cache
            ):  # first time key cases - empty dict if no matches => up to you to change threshold
                companies_cache[company.company_id] = {}
            if college.id not in colleges_cache:
                colleges_cache[college.id] = {}

            if dist <= threshold:  # if they are close enough together
                the_key = str(len(companies_cache[company.company_id]) + 1)
                companies_cache[company.company_id][the_key] = college.id

                the_key = str(len(colleges_cache[college.id]) + 1)
                colleges_cache[college.id][the_key] = company.company_id

    for company in companies:
        company.colleges_ids = json.dumps(companies_cache[company.company_id])

    for college in colleges:
        college.companies_ids = json.dumps(colleges_cache[college.id])
    db.session.commit()
    return


def populate_link_companies_colleges(db):
    """ populates the link_companies_colleges table """
    colleges = db.session.query(Colleges).all()

    for college in colleges:
        the_companies = json.loads(college.companies_ids)  # get dict (str -> int)

        for key in range(1, 1 + len(the_companies)):  # due to the json format style
            key = str(key)
            the_company_id = the_companies[key]
            the_college_id = college.id
            entry = {"company_id": the_company_id, "college_id": the_college_id}
            ins = models.link_companies_colleges.insert().values(entry)
            db.engine.execute(ins)
    # Note: db.session.commit() not needed due to engine
    return


if __name__ == "__main__":
    # put script here to run:
    # populate_link_companies_colleges(models.db)

    """
    # Drop/recreate db.
    drop_db(models.db)
    create_db(models.db)

    # by passing in business_cities and college_cities, visited_cities will only have cities that have BOTH 1+ businesses and 1+ colleges
    visited_cities = populate_cities(models.db, read_csv('../scraper/cities2.csv'), business_cities, college_cities) # newest cities csv
    populate_colleges(models.db, read_csv('../scraper/colleges4.csv'), visited_cities)

    visited_cities = set(map(lambda x : x[0], models.db.session.query(Cities.name).all()))
    Companies.__table__.drop(models.db.session.bind)
    Companies.__table__.create(models.db.session.bind)
    populate_companies(models.db, read_csv('../scraper/parsed_bizniz.csv'), visited_cities)
    """

    """
    from collections import defaultdict

    companies = models.db.session.query(Companies.company_id, Companies.city_id).all()
    colleges = models.db.session.query(Colleges.id, Colleges.city_id).all()

    city_to_college = defaultdict(list)
    city_to_company = defaultdict(list)

    for company, city in companies:
        city_to_company[city].append(company)

    for college, city in colleges:
        city_to_college[city].append(college)

    # print(city_to_company)
    # print()
    # print(city_to_college)

    print(len(city_to_college))
    print(len(city_to_company))
    """
