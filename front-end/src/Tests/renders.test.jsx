import React from "react"
import { configure, shallow, mount } from "enzyme"
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import ReactDOM from 'react-dom';
import Business from "../pages/Business"
import About from "../pages/About";
import College from "../pages/College"
import Home from "../pages/Home"
import Location from "../pages/Location"
import App from "../App";
import Void from "../pages/Void";
import { BrowserRouter as Router, Route } from "react-router-dom"
import { QueryParamProvider } from "use-query-params"
configure({ adapter: new Adapter() })

window.URL.createObjectURL = function() {};
jest.mock('mapbox-gl/dist/mapbox-gl', () => ({
    App: () => ({}),
}));

//Used format of testing from Cultured Foodies Group from Spring
//2021, link to Cultured Foodies Gitlab Repo: https://gitlab.com/cs373-group-11/cultured-foodies/-/tree/master
describe("Test Pages to render", () => {
    it('no crash on site', () => {
        const div = document.createElement('div');
        ReactDOM.render(
        <Router>
            <QueryParamProvider ReactRouterRoute={Route}>
                <App />
            </QueryParamProvider>
        </Router>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    //check instance models
    test('Check locations not null', () => {
        const comp = shallow(
        <Router>
            <QueryParamProvider ReactRouterRoute={Route}>
                <Location />
            </QueryParamProvider>
        </Router>
        );
        expect(comp).not.toBeUndefined;
        expect(comp).toHaveLength(1);
    });
    test('Check college not null', () => {
        const comp = shallow(
            <Router>
                <QueryParamProvider ReactRouterRoute={Route}>
                    <College />
                </QueryParamProvider>
            </Router>
        );
        expect(comp).not.toBeUndefined;
        expect(comp).toHaveLength(1);
    });
    test('Check business not null', () => {
        const comp = shallow(
        <Router>
            <QueryParamProvider ReactRouterRoute={Route}>
                <Business />
            </QueryParamProvider>
        </Router>
        );
        expect(comp).not.toBeUndefined;
        expect(comp).toHaveLength(1);
    });

    //check if anything has changed
    test("Business", () => {
        const businessTest = shallow(
        <Router>
            <QueryParamProvider ReactRouterRoute={Route}>
                <Business />
            </QueryParamProvider>
        </Router>
        )
        expect(businessTest).toMatchSnapshot()
    })
    test("Void", () => {
        const voidTest = shallow(<Void />)
        expect(voidTest).toMatchSnapshot()
    })
    test("About", () => {
        const aboutTest = shallow(<About />)
        expect(aboutTest).toMatchSnapshot()
    })
    test("College", () => {
        const collegeTest = shallow(
            <Router>
                <QueryParamProvider ReactRouterRoute={Route}>
                    <College />
                </QueryParamProvider>
            </Router>
        );
        expect(collegeTest).toMatchSnapshot()
    })
    test("Home", () => {
        const homeTest = shallow(<Home />)
        expect(homeTest).toMatchSnapshot()
    })
    test("Location", () => {
        const locationTest = shallow(
            <Router>
                <QueryParamProvider ReactRouterRoute={Route}>
                    <Location />
                </QueryParamProvider>
            </Router>
        )
        expect(locationTest).toMatchSnapshot()
    })

})