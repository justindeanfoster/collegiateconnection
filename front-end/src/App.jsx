import React from 'react';
import PageMenu from './components/Navbar'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

// View all pages.
import Home from './pages/Home';
import Business from './pages/Business'
import Location from './pages/Location'
import About from './pages/About'
import College from './pages/College'
import SearchTab from './pages/Search'
import Visualization from './pages/Visualizations';
import { QueryParamProvider } from "use-query-params"


// Import instance pages


import Void from './pages/Void';

import CollegeInstance from './pages/CollegeInstance';
import BusinessInstance from './pages/BusinessInstance';
import CityInstance from './pages/CityInstance';

import { useEffect } from "react";
import { useLocation } from "react-router-dom";

// Scroll restoration snippet taken from: https://reactrouter.com/web/guides/scroll-restoration
function ScrollToTop() {
    const { pathname } = useLocation();

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [pathname]);

    return null;
}


const Routes = () => (
    <Switch>
        <Route exact path='/'>
            <Home />
        </Route>
        <Route exact path='/home'>
            <Home />
        </Route>
        <Route exact path='/locations'>
            <Location />
        </Route>
        <Route exact path='/companies'>
            <Business />
        </Route>
        <Route exact path='/about'>
            <About />
        </Route>
        <Route exact path='/colleges'>
            <College />
        </Route>
        <Route exact path='/search'>
            <SearchTab />
        </Route>
        <Route exact path='/void'>
            <Void />
        </Route>

        <Route exact path='/visualizations'>
            <Visualization/>
        </Route>

        <Route
            exact path='/colleges/:id'
            component={CollegeInstance}
        />
        <Route
            exact path='/companies/:id'
            component={BusinessInstance}
        />
        <Route
            exact path='/locations/:id'
            component={CityInstance}
        />

    </Switch>
)
function App() {
    return (
        <>
            <ScrollToTop />
            <div className='app'>
                <PageMenu></PageMenu>
                <Routes></Routes>
            </div>
        </>
    );
}
export default App
