import { React, useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./styles/location.css";
import {
  Pagination,
  PageHeader,
  Input,
  Spin,
  Select,
  Typography,
  Slider,
  Collapse,
  Empty,
} from "antd";
import { Descriptions } from "antd";
import CollegeCard from "../components/CollegeCard";
import { view_all_colleges } from "../library/client";
import {
  Row,
  Col,
  Divider,
} from "antd";
import { DownOutlined } from "@ant-design/icons";
import stateNames from "./static/States.json";
import Filter from "../components/Filter";
import {
  StringParam,
  NumberParam,
  useQueryParams,
  withDefault,
} from "use-query-params";

import { useHistory } from "react-router";
const { Search } = Input;
const { Option } = Select;
const { Title } = Typography;
const Panel = Collapse.Panel;

const value_mapping_sort = {
  sat: "SAT (Asc.)",
  "-sat": "SAT (Desc.)",
  act: "ACT (Asc.) ",
  "-act": "ACT (Desc.)",
  accept_rate: "Acceptance Rate (Asc.)",
  "-accept_rate": "Acceptance Rate (Desc.)",
  in_state_tuition: "In-State Tuition (Asc.)",
  "-in_state_tuition": "In-State Tuition (Desc.)",
  out_state_tuition: "Out-of-State Tuition (Asc.)",
  "-out_state_tuition": "Out-of-State Tuition (Desc.)",
};

/* Pagination code inspired by: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Politicians/GridView.js. */
export default function College() {
  const [loading, setLoading] = useState(true);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(20);
  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    perPage: withDefault(NumberParam, 20),
    search: StringParam,
    sort: StringParam,
    sat: StringParam,
    act: StringParam,
    public: StringParam,
    accept_rate: StringParam,
    in_state_tuition: StringParam,
    out_state_tuition: StringParam,
    state: StringParam,
  });
  const [collegesData, setCollegesData] = useState([]);
  const [highlight, setHighlight] = useState([]);

  const history = useHistory();

  const parseRange = (rangeStr, defaultLow, defaultHigh) => {
    if (rangeStr === undefined || rangeStr === null) {
      return [defaultLow, defaultHigh];
    }
    const [lower, upper] = rangeStr.split("-");
    return [parseInt(lower), parseInt(upper)];
  };

  useEffect(() => {
    const data = async () => {
      try {
        setLoading(true);
        const fetched = await view_all_colleges(page, perPage, params);
        setLoading(false);
        setCollegesData(fetched.colleges || []);
        setTotal(fetched.total);
      } catch (e) {
        history.push("/void");
      }
    };

    data();
  }, [page, perPage, params, highlight]);

  const customPanelStyle = {
    borderRadius: "30px",
    border: 0,
    overflow: "hidden",
  };
  // contains all information needed to display the college instance
  return (
    <>
      <div className="page-header-wrapper">
        <PageHeader
          ghost={false}
          title={<b className="bolded"> Explore Colleges </b>}
          subTitle="Browse/search for available colleges"
        >
          <Descriptions size="small" column={3}>
            <Descriptions>
              Learn more about the school of your dreams!
            </Descriptions>
          </Descriptions>
          <Search
            placeholder={params.search ? params.search : "Search"}
            allowClear
            enterButton="Search"
            size="large"
            onSearch={(value) => {
              setParams({ ...params, search: value, page: 1 });
              setHighlight(value.split(" "));
            }}
          />
          <Divider />
          <div style={{ paddingBottom: "10px" }}>
            <Title level={5}>
              {" "}
              <i> Search Customization </i>{" "}
            </Title>
          </div>
          <Row>
            <Col>
              <div
                style={{
                  display: "inline",
                  paddingTop: "10px",
                  paddingRight: "10px",
                }}
              >
                <div>
                  <b>Sorting</b>
                </div>
                <div style={{ paddingTop: "10px", paddingRight: "10px" }}>
                  <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder={
                      params.sort ? value_mapping_sort[params.sort] : "Sort by"
                    }
                    onSelect={(val) => {
                      if (val === null) {
                        let another = {};
                        for (let prop in params) {
                          if (prop === "page") {
                            another[prop] = 1;
                          } else if (prop === "sort") {
                            another[prop] = undefined;
                          } else {
                            another[prop] = params[prop];
                          }
                        }
                        setParams(another);
                        return;
                      }
                      setParams({ ...params, sort: val, page: 1 });
                    }}
                  >
                    <Option value={null}> Default </Option>
                    <Option value="sat">SAT (Asc.) </Option>
                    <Option value="-sat">SAT (Desc.) </Option>
                    <Option value="act">ACT (Asc.) </Option>
                    <Option value="-act">ACT (Desc.) </Option>
                    <Option value="accept_rate">Acceptance Rate (Asc.) </Option>
                    <Option value="-accept_rate">
                      Acceptance Rate (Desc.){" "}
                    </Option>
                    <Option value="in_state_tuition">
                      {" "}
                      In-State Tuition (Asc.){" "}
                    </Option>
                    <Option value="-in_state_tuition">
                      {" "}
                      In-State Tuition (Desc.){" "}
                    </Option>
                    <Option value="out_state_tuition">
                      {" "}
                      Out-of-State Tuition (Asc.){" "}
                    </Option>
                    <Option value="-out_state_tuition">
                      {" "}
                      Out-of-State Tuition (Desc.){" "}
                    </Option>
                  </Select>
                </div>
              </div>
            </Col>
            <Col>
              <div>
                {" "}
                <b> Private/Public </b>{" "}
              </div>
              <Filter
                options={[
                  ["Public", "true"],
                  ["Private", "false"],
                  ["All", null],
                ]}
                placeholder={
                  params.public
                    ? params.public === "true"
                      ? "Public"
                      : "Private"
                    : "Public/Private"
                }
                params={params}
                setParams={setParams}
                query={"public"}
                setPage={setPage}
              />
            </Col>
            <Col>
              <div>
                {" "}
                <b> State </b>{" "}
              </div>
              <Filter
                options={stateNames
                  .map((data) => [data.name, data.name])
                  .concat([["All", null]])}
                placeholder={params.state ? params.state : "States"}
                params={params}
                setParams={setParams}
                query={"state"}
                setPage={setPage}
              />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Collapse bordered={false}>
                <Panel header="Admissions" key="2" style={customPanelStyle}>
                  <div>
                    {" "}
                    <b> SAT </b>{" "}
                  </div>
                  <Slider
                    range
                    min={400}
                    max={1600}
                    step={10}
                    defaultValue={parseRange(params.sat, 400, 1600)}
                    disabled={false}
                    marks={{
                      400: "400",
                      700: "700",
                      1000: "1000",
                      1300: "1300",
                      1600: "1600",
                    }}
                    onAfterChange={(val) => {
                      let [lower, upper] = val;
                      setParams({
                        ...params,
                        sat: `${lower}-${upper}`,
                        page: 1,
                      });
                    }}
                  />

                  <br />

                  <div>
                    {" "}
                    <b> ACT </b>{" "}
                  </div>
                  <Slider
                    range
                    min={0}
                    max={36}
                    step={1}
                    defaultValue={parseRange(params.act, 0, 36)}
                    disabled={false}
                    marks={{ 0: "0", 9: "9", 18: "18", 27: "27", 36: "36" }}
                    onAfterChange={(val) => {
                      let [lower, upper] = val;
                      setParams({
                        ...params,
                        act: `${lower}-${upper}`,
                        page: 1,
                      });
                    }}
                  />
                  <br />

                  <div>
                    {" "}
                    <b> Acceptance Rate </b>{" "}
                  </div>
                  <Slider
                    range
                    min={0}
                    max={100}
                    step={1}
                    defaultValue={parseRange(params.accept_rate, 0, 100)}
                    disabled={false}
                    marks={{ 0: "0%", 100: "100%" }}
                    onAfterChange={(val) => {
                      let [lower, upper] = val;
                      setParams({
                        ...params,
                        accept_rate: `${lower}-${upper}`,
                        page: 1,
                      });
                    }}
                  />
                </Panel>
                <Panel header="Tuition" key="3" style={customPanelStyle}>
                  <div>
                    {" "}
                    <b> In-State Tuition </b>{" "}
                  </div>
                  <Slider
                    range
                    min={0}
                    max={80000}
                    step={1000}
                    defaultValue={parseRange(params.in_state_tuition, 0, 80000)}
                    disabled={false}
                    marks={{ 0: "$0", 80000: "$80k" }}
                    onAfterChange={(val) => {
                      let [lower, upper] = val;
                      setParams({
                        ...params,
                        in_state_tuition: `${lower}-${upper}`,
                        page: 1,
                      });
                    }}
                  />
                  <br />

                  <div>
                    {" "}
                    <b> Out-of-State Tuition </b>{" "}
                  </div>
                  <Slider
                    range
                    min={0}
                    max={80000}
                    step={1000}
                    defaultValue={parseRange(
                      params.out_state_tuition,
                      0,
                      80000
                    )}
                    disabled={false}
                    marks={{ 0: "$0", 80000: "$80k" }}
                    onAfterChange={(val) => {
                      let [lower, upper] = val;
                      setParams({
                        ...params,
                        out_state_tuition: `${lower}-${upper}`,
                        page: 1,
                      });
                    }}
                  />
                  <br />
                </Panel>
              </Collapse>
            </Col>
          </Row>
        </PageHeader>
      </div>
      {loading ? (
        <div style={{ textAlign: "center" }}>
          {" "}
          <Spin size="large" tip="loading colleges..." />{" "}
        </div>
      ) : (
        <>
          {collegesData.length > 0 ? (
            <div className="grid-container">
              {collegesData.map((college) => (
                <div className="grid-item">
                  {" "}
                  <CollegeCard
                    data={{ ...college, searchString: params.search }}
                  />{" "}
                </div>
              ))}
            </div>
          ) : (
            <div style={{ textAlign: "center" }}>
              {" "}
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />{" "}
            </div>
          )}
          <div className="paddingWrapper">
            <Pagination
              showSizeChanger
              total={total}
              pageSize={params.perPage}
              current={params.page}
              onChange={(num, size) =>
                setParams({ ...params, page: num, perPage: size })
              }
              style={{
                padding: "30px",
                display: "flex",
                justifyContent: "flex-end",
              }}
              showTotal={(total) => `${total} Colleges`}
            />
          </div>
        </>
      )}
    </>
  );
}
