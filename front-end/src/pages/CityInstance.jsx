import { React, useState, useEffect } from "react";
import "antd/dist/antd.css";
import "./styles/location.css";
import {
  PageHeader,
  Popover,
  Spin,
  Card,
  Divider,
  Typography,
  Progress,
  Avatar,
  Button,
  Table,
} from "antd";
import { view_city, view_college, view_company } from "../library/client";
import CollegeCard from "../components/CollegeCard";
import "./styles/storeList.css";
import { useLocation } from "react-router-dom";
import "./styles/storeInstance.css";
import "../components/styles/card.css";
import Meta from "antd/lib/card/Meta";
import "bootstrap/dist/css/bootstrap.min.css";
import MapContainer from "../components/MapContainer";
import { Link, useHistory } from "react-router-dom";

const { Title } = Typography;
const columns = [
  {
    title: "Logo",
    dataIndex: "logo",
    key: "logo",
    render: (link) => {
      if (link === null || link === undefined) {
        return (
          <Avatar
            size={64}
            src="https://www.wodonnell.com/wp-content/uploads/2019/02/business-placeholder.png"
          />
        );
      }
      return <Avatar size={64} src={link} />;
    },
  },
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Alexa Rank",
    dataIndex: "alexa_rank",
    key: "alexa_rank",
    render: (data) => (data ? data : "N/A"),
  },
  {
    title: "Employees",
    dataIndex: "employees",
    key: "employees",
    render: (data) => (data ? data : "N/A"),
  },
  {
    title: "Industry",
    dataIndex: "industry",
    key: "industry",
    render: (data) => (data ? data : "N/A"),
  },
  {
    title: "Annual Revenue (estimated)",
    dataIndex: "estimated_annual_revenue",
    key: "estimated_annual_revenue",
    render: (data) => (data ? data : "N/A"),
  },
  {
    title: "Headquarters Location (latitude)",
    dataIndex: "location_latitude",
    key: "location_latitude",
  },
  {
    title: "Headquarters Location (longitude)",
    dataIndex: "location_longitude",
    key: "location_longitude",
  },
  {
    title: "Learn more",
    dataIndex: "company_id",
    key: "company_id",
    render: (id) => {
      return (
        <Link
          to={{
            pathname: `/companies/${id}`,
            state: { identifier: id },
          }}
        >
          <Button
            style={{ background: "#90EE90", borderColor: "#90EE90" }}
            type="primary"
            shape="round"
          >
            {" "}
            More{" "}
          </Button>
        </Link>
      );
    },
  },
];

const CityInfo = (data) => {
  return (
    <>
      <div>
        <div className="container-fluid">
          <div className="row g-5 justify-content-md-center">
            <div className="col-xs-12 col-sm-12 col-md-5 col-lg-4">
              <div className="p-3 text-left">
                <Card
                  cover={
                    <img
                      alt="City"
                      src={data.mobile_image}
                      className="college-image"
                      style={{
                        padding: "15px",
                      }}
                    />
                  }
                  className="store-info-card"
                >
                  <Meta
                    title={<Title level={3}> {data.name} </Title>}
                    description={
                      <>
                        <b> City: </b> {data.full_name} <br />
                        <b> Population: </b> {data.population || "N/A"} <br />
                        <b> State: </b> {data.state} <br />
                        <b> Latitude: </b> {data.latitude} <br />
                        <b> Longitude: </b> {data.longitude} <br />
                        <b> Timezone: </b> {data.timezone} <br />
                      </>
                    }
                    style={{ textAlign: "center" }}
                  />
                </Card>
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
              <div className="p-3 text-center">
                <Card className="store-info-card">
                  <Popover
                    title="What is this?"
                    content="Quality of life metrics give by Teleport."
                  >
                    <Title level={3} style={{ cursor: "pointer" }}>
                      {" "}
                      City Indices{" "}
                    </Title>
                  </Popover>
                  <div className="grid-container-sm">
                    <div className="grid-item-small">
                      <Title level={5}> Travel Connectivity</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.travel_connectivity * 10).toPrecision(2)}
                        format={(percent) =>
                          percent
                            ? `${data.travel_connectivity.toPrecision(2)}/10`
                            : "N/A"
                        }
                      />
                    </div>

                    <div className="grid-item-small">
                      <Title level={5}> Housing</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.housing * 10).toPrecision(2)}
                        format={(percent) =>
                          percent ? `${data.housing.toPrecision(2)}/10` : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Economy</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.economy * 10).toPrecision(2)}
                        format={(percent) =>
                          percent ? `${data.economy.toPrecision(2)}/10` : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Business Freedom</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.business_freedom * 10).toPrecision(2)}
                        format={(percent) =>
                          percent
                            ? `${data.business_freedom.toPrecision(2)}/10`
                            : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Tolerance</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.tolerance * 10).toPrecision(2)}
                        format={(percent) =>
                          percent
                            ? `${data.tolerance.toPrecision(2)}/10`
                            : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Outdoors</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.outdoors * 10).toPrecision(2)}
                        format={(percent) =>
                          percent ? `${data.outdoors.toPrecision(2)}/10` : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Commute</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.commute * 10).toPrecision(2)}
                        format={(percent) =>
                          percent ? `${data.commute.toPrecision(2)}/10` : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Internet Access</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.internet_access * 10).toPrecision(2)}
                        format={(percent) =>
                          percent
                            ? `${data.internet_access.toPrecision(2)}/10`
                            : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Startups</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.startups * 10).toPrecision(2)}
                        format={(percent) =>
                          percent ? `${data.startups.toPrecision(2)}/10` : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Education</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.education * 10).toPrecision(2)}
                        format={(percent) =>
                          percent
                            ? `${data.education.toPrecision(2)}/10`
                            : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Safety</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.safety * 10).toPrecision(2)}
                        format={(percent) =>
                          percent ? `${data.safety.toPrecision(2)}/10` : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Cost of Living</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.cost_of_living * 10).toPrecision(2)}
                        format={(percent) =>
                          percent
                            ? `${data.cost_of_living.toPrecision(2)}/10`
                            : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Leisure Culture</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.leisure_culture * 10).toPrecision(2)}
                        format={(percent) =>
                          percent
                            ? `${data.leisure_culture.toPrecision(2)}/10`
                            : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Taxation</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.taxation * 10).toPrecision(2)}
                        format={(percent) =>
                          percent ? `${data.taxation.toPrecision(2)}/10` : "N/A"
                        }
                      />
                    </div>
                    <div className="grid-item-small">
                      <Title level={5}> Healthcare</Title>
                      <Progress
                        type="circle"
                        strokeColor={{
                          "0%": "#108ee9",
                          "100%": "#90EE90",
                        }}
                        percent={(data.healthcare * 10).toPrecision(2)}
                        format={(percent) =>
                          percent
                            ? `${data.healthcare.toPrecision(2)}/10`
                            : "N/A"
                        }
                      />
                    </div>
                  </div>
                </Card>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

/* Pagination code inspired by: https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Politicians/GridView.js. */
export default function CityInstance() {
  const { state } = useLocation();

  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({});

  const [loadingColleges, setLoadingColleges] = useState(true);
  const [collegesIds, setCollegesIds] = useState([]);

  const [loadingCompanies, setLoadingCompanies] = useState(true);
  const [companyIds, setCompanyIds] = useState([]);

  const history = useHistory();
  useEffect(() => {
    const fetcher = async () => {
      try {
        var id = undefined;
        if (state == undefined) {
          id = window.location.pathname.split("/")[2];
        } else {
          id = state.identifier;
        }
        const info = await view_city(id);
        setData(info);
        setLoading(false);

        let ids = [];
        let ids_obj = JSON.parse(info.colleges_ids);
        for (let [_, value] of Object.entries(ids_obj)) {
          ids.push(value);
          if (ids.length === 10) {
            break;
          }
        }
        const results = await Promise.all(ids.map((id) => view_college(id)));
        let all_objs = results.filter((result) => result != null);

        setCollegesIds(all_objs);
        setLoadingColleges(false);

        // Fetch for companies
        ids = [];
        ids_obj = JSON.parse(info.companies_ids);
        for (let [_, value] of Object.entries(ids_obj)) {
          ids.push(value);
          if (ids.length === 15) {
            break;
          }
        }
        all_objs = await Promise.all(ids.map((id) => view_company(id)));

        setCompanyIds(all_objs);
        setLoadingCompanies(false);
      } catch (e) {
        console.error(e);
        history.push("/void");
      }
    };
    fetcher();
  // }, []);
  });
  
  // JSX for the city instance pages
  return (
    <>
      <div className="store-list-page-header">
        <PageHeader
          ghost={false}
          onBack={() => window.history.back()}
          title={data.name}
          subTitle="City Profile"
        />
      </div>

      {loading ? (
        <div style={{ textAlign: "center" }}>
          <Spin size="large" tip={`Loading Information for City... `} />
        </div>
      ) : (
        <>
          <Divider>
            {" "}
            <Title level={3}> General Information </Title>{" "}
          </Divider>
          <CityInfo {...data} />
          <Divider>
            {" "}
            <Title level={3}> Map </Title>{" "}
          </Divider>
          <MapContainer
            {...{
              latitude: data.latitude,
              longitude: data.longitude,
              is_city: true,
            }}
          />
          <Divider>
            {" "}
            <Title level={3}> Universities in City </Title>{" "}
          </Divider>
          {loadingColleges ? (
            <div style={{ textAlign: "center" }}>
              <Spin
                size="large"
                tip={`Loading Information for Nearby Colleges... `}
              />
            </div>
          ) : (
            <>
              <div className="grid-container">
                {collegesIds.map((college) => (
                  <div className="grid-item">
                    {" "}
                    <CollegeCard data={{ ...college }} />{" "}
                  </div>
                )) || "Not Available"}
              </div>
            </>
          )}
          <Divider>
            {" "}
            <Title level={3}> Companies in City </Title>{" "}
          </Divider>
          {loadingCompanies ? (
            <div style={{ textAlign: "center" }}>
              <Spin
                size="large"
                tip={`Loading Information for Nearby Companies... `}
              />
            </div>
          ) : (
            <>
              <Table
                columns={columns}
                dataSource={companyIds}
                scroll={{ x: true }}
                pagination={{
                  defaultPageSize: 5,
                }}
              />
            </>
          )}
        </>
      )}
    </>
  );
}
