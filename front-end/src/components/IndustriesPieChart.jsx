import React, { useState, useEffect } from "react";
import { Spin } from "antd";
import { useHistory } from "react-router-dom";
import {
    PieChart, 
    Pie,
    Legend,
    Tooltip,
    Cell,
    ResponsiveContainer 
} from "recharts";
import { view_all_companies } from '../library/client';

function IndustriesPieChart() {

  const [loading, setLoading] = useState(true)
  const [pieData, setPieData] = useState([])
  const history = useHistory();
  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

  useEffect(() => {

    const data = async () => {
        try {
            setLoading(true)
            const fetched = await view_all_companies(1, 500)
            setLoading(false)
            const companies = fetched.companies
            console.log(fetched.companies)
            

            const industryTypes = companies
                .map(company => company.industry) // get all media types
                .filter((industry, index, array) => array.indexOf(industry) === index); // filter out duplicates
            const ret = industryTypes
                .map(industry => ({
                    name: industry,
                    value: companies.filter(item => item.industry === industry).length
                }));
        

            console.log(ret)
            setPieData(ret || [])
        } catch (e) {
            history.push('/void')
        }
    }

    data()


  }, [])

  return (
    loading ? (
        <div style={{ textAlign: 'center' }}> <Spin size='large' tip='loading visualization...'/> </div>
    ) :  <div style={{
        display: 'flex'
    }}>
        <ResponsiveContainer width='99%' height={500}>
        <PieChart width={400} height={400}>
          <Pie
            dataKey="value"
            isAnimationActive={false}
            data={pieData}
            cx="50%"
            cy="50%"
            outerRadius={150}
            fill="#8884d8"
            label
            
          >
          {pieData.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
          </Pie>
          {/* <Pie dataKey="value" data={pieData} cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d" /> */}
          <Tooltip />
        </PieChart>
        </ResponsiveContainer>
    </div>

  );
}

export default IndustriesPieChart;
