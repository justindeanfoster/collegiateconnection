import React from "react";
import "antd/dist/antd.css";
import "./styles/card.css";
import { Card, Popover, Progress, Tag } from "antd";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

const { Meta } = Card;

// Function to determine which tag to add to card (depending on SAT score).
const determine_tag = (sat_score) => {
  if (sat_score === undefined || sat_score === null) {
    return <Tag color="purple">N/A</Tag>;
  }
  if (sat_score >= 1450) {
    return <Tag color="green">{sat_score}</Tag>;
  }
  if (sat_score >= 1200) {
    return <Tag color="blue">{sat_score}</Tag>;
  }
  return <Tag color="red">{sat_score}</Tag>;
};

const CollegeCard = (params) => {
  const { data } = params;
  const history = useHistory();

  // Routing user to specific instance page.
  const handleClick = () => {
    history.push(`/colleges/${data.id}`);
  };
  return (
    <>
      <Card
        cover={
          <img
            alt="College"
            // Default placeholder image.
            src={
              data.image ||
              "https://cdn1.iconfinder.com/data/icons/location-basic-1-semi-gray/468/Layer11-512.png"
            }
            className="icon-image"
          />
        }
        className={data.className ? data.className : "itemcard"}
        onClick={handleClick}
      >
        <Meta
          title={
            // Highlight all words based on whitespaces if search string is not empty.
            // Otherwise, don't highlight anything.
            <Highlighter
              searchWords={
                data.searchString !== undefined && data.searchString !== null
                  ? data.searchString.split(" ")
                  : []
              }
              textToHighlight={data.name}
            />
          }
          description={
            <>
              <b> City: </b> <br />{" "}
              {
                <Highlighter
                  searchWords={
                    data.searchString !== undefined &&
                    data.searchString !== null
                      ? data.searchString.split(" ")
                      : []
                  }
                  textToHighlight={data.school_city}
                />
              }{" "}
              <br />
              <b> Number of Students: </b> <br /> {data.student_size || "N/A"}{" "}
              <br />
              <b>
                {" "}
                In State Tuition: <br />{" "}
              </b>{" "}
              {data.cost_tuition_in_state || "N/A"} <br />
              <b>
                {" "}
                Out of State Tuition: <br />{" "}
              </b>{" "}
              {data.cost_tuition_out_of_state || "N/A"} <br />
              <b> Acceptance Rate: </b> <br />{" "}
              {data.admissions_admission_rate_overall ? (
                <Popover
                  content={`${(
                    data.admissions_admission_rate_overall * 100
                  ).toPrecision(2)}%`}
                >
                  <Progress
                    percent={data.admissions_admission_rate_overall * 100}
                    strokeColor={{
                      "0%": "#108ee9",
                      "100%": "#90EE90",
                    }}
                    showInfo={false}
                  />
                </Popover>
              ) : (
                "N/A"
              )}{" "}
              <br />
              <b> Retention Rate: </b> <br />{" "}
              {data.student_retention_rate_four_year_full_time_pooled ? (
                <Popover
                  content={`${(
                    data.student_retention_rate_four_year_full_time_pooled * 100
                  ).toPrecision(2)}%`}
                >
                  <Progress
                    percent={
                      data.student_retention_rate_four_year_full_time_pooled *
                      100
                    }
                    strokeColor={{
                      "0%": "#108ee9",
                      "100%": "#90EE90",
                    }}
                    showInfo={false}
                  />
                </Popover>
              ) : (
                "N/A"
              )}{" "}
              <br />
              <b> Average SAT: </b> <br />{" "}
              {determine_tag(data.admissions_sat_scores_average_overall)}
            </>
          }
        />
      </Card>
    </>
  );
};
export default CollegeCard;
