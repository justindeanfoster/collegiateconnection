import "antd/dist/antd.css";
import "./styles/navbar.css";
import { Affix, Menu } from "antd";
import { Link } from "react-router-dom";
import {
  UserOutlined,
  ShoppingOutlined,
  EnvironmentOutlined,
  HomeOutlined,
  CoffeeOutlined,
  SearchOutlined,
  BarChartOutlined,
} from "@ant-design/icons";

// Navbar, each menu item links to a view all page/search/visualization.
const PageMenu = () => (
  <>
    <Affix>
      <Menu mode="horizontal">
        <Menu.Item key="logo" className="logo">
          <Link className="link" to="/home">
            <b className="bolded">Collegiate</b>Connection
          </Link>
        </Menu.Item>
        <Menu.Item key="home" icon={<HomeOutlined />} className="customclass">
          <Link className="link" to="/home">
            Home
          </Link>
        </Menu.Item>
        <Menu.Item key="about" icon={<UserOutlined className="icon" />}>
          <Link className="link" to="/about">
            About Us
          </Link>
        </Menu.Item>
        <Menu.Item key="colleges" icon={<CoffeeOutlined className="icon" />}>
          <Link className="link" to="/colleges">
            Universities
          </Link>
        </Menu.Item>
        <Menu.Item key="companies" icon={<ShoppingOutlined />}>
          <Link className="link" to="/companies">
            Companies
          </Link>
        </Menu.Item>
        <Menu.Item key="zip Codes" icon={<EnvironmentOutlined />}>
          <Link className="link" to="/locations">
            Cities
          </Link>
        </Menu.Item>
        <Menu.Item key="search" icon={<SearchOutlined />}>
          <Link className="link" to="/search">
            Search
          </Link>
        </Menu.Item>
        <Menu.Item key="visualizations" icon={<BarChartOutlined />}>
          <Link className="link" to="/visualizations">
            Visualizations
          </Link>
        </Menu.Item>
      </Menu>
    </Affix>
  </>
);

export default PageMenu;
