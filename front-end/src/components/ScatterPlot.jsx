import React, { useState, useEffect } from "react";
import { Spin } from "antd";
import { useHistory } from "react-router-dom";
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  ZAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import { view_all_colleges } from "../library/client";

function ScatterPlot() {
  const [loading, setLoading] = useState(true);
  const [plotData, setPlotData] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const data = async () => {
      try {
        setLoading(true);
        const fetched = await view_all_colleges(1, 500, {
          sat: "0-1600",
          in_state_tuition: "0-80000",
          out_state_tuition: "0-80000",
        });
        setLoading(false);
        console.log(fetched.colleges);

        const ret = fetched.colleges.map((college) => {
          let obj = {};
          obj.id = college.id;
          obj.state_name = college.state_name;
          obj.name = college.name;
          obj.sat = college.admissions_sat_scores_average_overall;
          obj.cost = Math.floor(
            (college.cost_tuition_in_state +
              college.cost_tuition_out_of_state) /
            2
          );

          return obj;
        });

        // console.log(ret);
        setPlotData(ret || []);
      } catch (e) {
        history.push("/void");
      }
    };

    data();
  }, []);

  return loading ? (
    <div style={{ textAlign: "center" }}>
      {" "}
      <Spin size="large" tip="loading visualization..." />{" "}
    </div>
  ) : (
    <div
      style={{
        display: "flex",
      }}
    >
      <ResponsiveContainer width="99%" height={500}>
        <ScatterChart width={600} height={600} data={plotData}>
          <CartesianGrid />
          <XAxis
            tickFormatter={(value) => new Intl.NumberFormat("en").format(value)}
            type="number"
            dataKey="cost"
            name="Tuition Cost"
            label={{
              value: "Average Tuition Cost ($)",
              position: "insideBottom",
              offset: -5,
            }}
          />
          <YAxis
            type="number"
            dataKey="sat"
            name="Average SAT score"
            label={{
              value: "Average SAT score",
              angle: -90,
              offset: 10,
            }}
            domain={[0, 1600]}
            tickCount={5}
          />
          <ZAxis type="category" dataKey="name" name="College" />
          <Tooltip labelFormatter={(t) => t} />
          <Scatter
            data={plotData}
            fill="#90EE90"
            style={{ cursor: "pointer" }}
          />
        </ScatterChart>
      </ResponsiveContainer>
    </div>
  );
}

export default ScatterPlot;
