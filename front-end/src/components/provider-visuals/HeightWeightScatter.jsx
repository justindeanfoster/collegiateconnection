import React, { useState, useEffect } from "react";
import {
    ScatterChart,
    Scatter,
    XAxis,
    YAxis,
    ZAxis,
    CartesianGrid,
    Tooltip,
    ResponsiveContainer,
} from "recharts";
import { useHistory } from "react-router-dom";

import axios from "axios";

const view_players_baseball = async () => {
    const query = 'https://api.citieslovebaseball.me/player?pageSize=500'
    const data = (
        await axios.get(query)
    ).data.players

    return data
}
function HWScatter() {

    const [loading, setLoading] = useState(true)
    const [plotData, setPlotData] = useState([])
    const history = useHistory();

    useEffect(() => {

        const data = async () => {
            try {
                setLoading(true)
                const fetched = await view_players_baseball()
                setLoading(false)

                const ret = fetched.map(player => {
                    let obj = {}
                    obj.name = player.name
                    obj.raw_height = player.height
                    var x = Number(player.height.charAt(0))
                    var y = Number(player.height.charAt(3))
                    obj.height = (x * 12) + y
                    obj.weight = player.weight
                    return obj;
                })

                console.log(ret)
                setPlotData(ret || [])
            } catch (e) {
                console.error(e)
                history.push('/void')
            }
        }

        data()

    }, [])
    return (
        <ResponsiveContainer width="99%" height={500}>
            <ScatterChart width={600} height={600} data={plotData}>
                <CartesianGrid />
                <XAxis
                    tickFormatter={(value) => new Intl.NumberFormat("en").format(value)}
                    type="number"
                    dataKey="height"
                    name="Total inches"
                    label={{
                        value: "Height(in)",
                        position: "insideBottom",
                        offset: -5,
                    }}
                    domain={[60, 80]}
                />
                <YAxis
                    type="number"
                    dataKey="weight"
                    name="Weight"
                    label={{
                        value: "Weight(lbs)",
                        angle: -90,
                        offset: 10,
                    }}
                    domain={[140, 300]}
                    tickCount={5}
                />
                <ZAxis type="category" dataKey="name" name="player" />
                <Tooltip labelFormatter={(t) => t} />
                <Scatter
                    data={plotData}
                    fill="turquoise"
                    style={{ cursor: "pointer" }}
                />
            </ScatterChart>
        </ResponsiveContainer>

    );
}
export default HWScatter;