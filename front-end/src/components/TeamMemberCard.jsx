import React from "react";
import "antd/dist/antd.css";
import "./styles/card.css";
import { Card } from "antd";

const { Meta } = Card;
class TeamMemberCard extends React.Component {
  render() {
    return (
      <>
        <Card
          cover={
            <img
              alt="Team member"
              src={this.props.image}
              className="croppedImage"
            />
          }
          className="teamcard"
        >
          <Meta
            title={this.props.name}
            description={
              <div>
                <b> Email: </b> {this.props.email} <br />
                <b> Gitlab ID: </b> {this.props.username} <br />
                <b> Commits: </b> {this.props.commits} <br />
                <b> Issues: </b> {this.props.issues} <br />
                <b> Tests: </b> {this.props.tests} <br />
                <b> Role: </b> {this.props.role} <br />
                <b> Bio: </b> {this.props.bio}
              </div>
            }
          />
        </Card>
      </>
    );
  }
}
export default TeamMemberCard;
