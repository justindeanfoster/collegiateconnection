import React from "react";
// import GoogleMapReact from "google-map-react";
import "./styles/mapcontainer.css";
// import { Icon } from "@iconify/react";
// import locationIcon from "@iconify/icons-mdi/map-marker";
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import mapboxgl from "mapbox-gl";

// To allow for displaying of map when deployed to production.

try {
  // eslint-disable-next-line import/no-webpack-loader-syntax
  mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;
} catch (error) {
  console.error(error);
}

const Map = ReactMapboxGl({
  accessToken:
    "pk.eyJ1Ijoia2V2aW5hbGk2NCIsImEiOiJja3cxM2hlajExNnhzMnVvYjJ6bDZqczczIn0.IxnKEuXqDjjKp2YtiWCaZw",
});

class MapContainer extends React.Component {
  render() {
    return (
      <div style={{ height: "500px", width: "auto", margin: "auto" }}>
        <Map
          // Custom map style.
          style="mapbox://styles/kevinali64/ckw182vev7ugs14p874zzkldr"
          id="marker"
          containerStyle={{
            height: "55vh",
            padding: "10px",
          }}
          center={[this.props.longitude, this.props.latitude]}
          // Zoom further out if this is a city map, otherwise default is fine.
          zoom={
            this.props.is_city === undefined || this.props.is_city === null
              ? [14]
              : [10]
          }
        >
          <Layer
            type="circle"
            id="marker"
            paint={{
              "circle-color": "#90EE90",
              "circle-stroke-width": 2,
              "circle-stroke-color": "#90EE90",
              "circle-stroke-opacity": 1,
            }}
          >
            <Feature
              coordinates={[this.props.longitude, this.props.latitude]}
            />
          </Layer>
        </Map>
      </div>
    );
  }
}
export default MapContainer;
