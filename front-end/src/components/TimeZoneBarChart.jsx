import React, { useState, useEffect} from "react";
// import { Spin } from "antd";
import { useHistory } from "react-router-dom";
// import axios from "axios";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';


import { view_all_cities } from "../library/client";

function Chart() {
    const [_, setLoading] = useState(true);
    const [plotData, setPlotData] = useState([]);
    const history = useHistory();

    useEffect(() => {
        const data = async () => {
            try {
                setLoading(true);
                const fetched = await view_all_cities(1, 500)
                const cities = fetched.cities

                setLoading(false);



                const timezones = cities
                    .map(city => city.timezone)
                    .filter((timezones, index, array) => array.indexOf(timezones) === index); // filter out duplicates

                const ret = timezones
                    .map(timezone => ({
                        type: timezone,
                        count: cities.filter(item => item.timezone === timezone).length
                    }));


                setPlotData(ret || []);
            } catch (e) {
                history.push("/void");
            }
        };

        data();
    }, []);
    
    console.log(plotData)
    return (
        <ResponsiveContainer width='99%' height={500}>
            <BarChart
                width={800}
                height={600}
                data={plotData}
                layout="horizontal"
                margin={{ bottom: 10 }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                    type="category"
                    dataKey="type"
                    tick={false}
                    label={{
                        value: 'TimeZones'
                    }}
                />
                <YAxis
                    type="number"
                    dataKey="count"
                    label={{
                        value: "count",
                        angle: -90,
                        position: "insideLeft"
                    }}
                />
                <Tooltip />
                <Bar dataKey="count" fill="maroon" />
            </BarChart>
        </ResponsiveContainer>
    );

}
export default Chart;