import React from "react";
import "antd/dist/antd.css";
import "./styles/card.css";
import { Card } from "antd";
import { useHistory } from "react-router-dom";
import Highlighter from "react-highlight-words";

const { Meta } = Card;

const PLACEHOLDER =
  "https://www.abcnorcal.org/wp-content/uploads/2019/05/company_logo.png";

function LocationCard(params) {
  const { data } = params;
  const history = useHistory();
  const handleClick = () => {
    history.push(`/locations/${data.id}`);
  };
  return (
    <>
      <Card
        cover={
          <img
            alt="City"
            src={data.mobile_image ? data.mobile_image : PLACEHOLDER}
            className="croppedImage"
          />
        }
        className={data.className ? data.className : "itemcard"}
        onClick={handleClick}
      >
        <Meta
          title={
            <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={data.searchString || []}
              textToHighlight={data.name}
            />
          }
          description={
            <div>
              <b style={{ textDecoration: "none !important" }}> State: </b>{" "}
              {
                <Highlighter
                  highlightClassName="YourHighlightClass"
                  searchWords={data.searchString || []}
                  textToHighlight={data.state}
                />
              }{" "}
              <br />
              <b> Latitude: </b>{" "}
              {
                <Highlighter
                  highlightClassName="YourHighlightClass"
                  searchWords={data.searchString || []}
                  textToHighlight={data.latitude.toString()}
                />
              }{" "}
              <br />
              <b> Longitude: </b>{" "}
              {
                <Highlighter
                  highlightClassName="YourHighlightClass"
                  searchWords={data.searchString || []}
                  textToHighlight={data.longitude.toString()}
                />
              }{" "}
              <br />
              <b> Timezone: </b>{" "}
              {
                <Highlighter
                  highlightClassName="YourHighlightClass"
                  searchWords={data.searchString || []}
                  textToHighlight={data.timezone}
                />
              }{" "}
              <br />
              <b> Economy: </b> {data.economy.toPrecision(3)} <br />
              <b> Cost of Living: </b> {data.cost_of_living.toPrecision(3)}{" "}
              <br />
              <b> Housing: </b> {data.housing.toPrecision(3)}
            </div>
          }
        />
      </Card>
    </>
  );
}
export default LocationCard;
