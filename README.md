- Name, EID, and GitLab ID, of all members

|Name              |EID    |Gitlab ID        |
|---               |---    |---              |
|Justin Foster     |jdf3434|justindeanfoster |
|Mathew Tucciarone|mat4987|mat4987          |
|Aditya Gupta      |ag68834|aditya2000       |
|Kevin Li          |kal3558|li.kevin         |
|William Kim       |sk45248|william94kr      |

- Git SHA: 09cec0c8feca1a54e371f2d2e297cb101b4dcc27


|Phase             |Project Leader   |
|---               |---    |
|Phase 1           |Justin Foster|
|Phase 2           |William Kim|
|Phase 3           |Aditya Gupta|
|Phase 4           |Mathew Tucciarone|


- Link to GitLab pipelines: https://gitlab.com/justindeanfoster/collegiateconnection/-/pipelines

- Link to website: https://www.collegiateconnection.me

- Link to presentation: https://youtu.be/NBPFT25iOjA

- Estimated completion time for each member (hours: int)

**Phase 1**
|Name               |Hours|
|---                |---  |
|Justin Foster      |15   |
|Mattew Tucciarone |15   |
|Aditya Gupta       |20   |
|Kevin Li           |20   |
|William Kim        |20   |

- Actual completion time for each member (hours: int)

|Name               |Hours|
|---                |---  |
|Justin Foster      |  25  |
|Mathew Tucciarone  |  15  |
|Aditya Gupta       |  27  |
|Kevin Li           |  32  |
|William Kim        |  27  |

**Phase 2**

|Name               |Hours|
|---                |---  |
|Justin Foster      |25   |
|Mattew Tucciarone |25   |
|Aditya Gupta       |25   |
|Kevin Li           |25   |
|William Kim        |25   |

- Actual completion time for each member (hours: int)

|Name               |Hours|
|---                |---  |
|Justin Foster      |  30  |
|Mathew Tucciarone  |  27  |
|Aditya Gupta       |  35 |
|Kevin Li           |  50  |
|William Kim        |  30  |


**Phase 3**

|Name               |Hours|
|---                |---  |
|Justin Foster      |25   |
|Mattew Tucciarone  |25   |
|Aditya Gupta       |25   |
|Kevin Li           |25   |
|William Kim        |25   |

- Actual completion time for each member (hours: int)

|Name               |Hours|
|---                |---  |
|Justin Foster      |  20  |
|Mathew Tucciarone  |  14  |
|Aditya Gupta       |  24  |
|Kevin Li           |  22  |
|William Kim        |  25  |

**Phase 4**

|Name               |Hours|
|---                |---  |
|Justin Foster      |15   |
|Mattew Tucciarone  |15   |
|Aditya Gupta       |15   |
|Kevin Li           |15   |
|William Kim        |15   |

- Actual completion time for each member (hours: int)

|Name               |Hours|
|---                |---  |
|Justin Foster      |  12  |
|Mathew Tucciarone  |  10  |
|Aditya Gupta       |  12  |
|Kevin Li           |  15  |
|William Kim        |  12  |

Git SHA: 09cec0c8feca1a54e371f2d2e297cb101b4dcc27

Comments: We referenced code from previous years. Here are some repositories that we referenced:
- https://gitlab.com/forbesye/fitsbits/-/tree/master/
- https://gitlab.com/caitlinlien/cs373-sustainability
- https://gitlab.com/cs373-group-11/cultured-foodies/
- https://gitlab.com/bhaver/relocccate/


For filtering and sorting, we were heavily inspired by:
https://gitlab.com/forbesye/fitsbits/-/blob/master/back-end/Politician.py

For the ranged query parameters/sliders, we took inspiration from:
https://gitlab.com/cs373-group14/books4u/-/tree/master/backend
